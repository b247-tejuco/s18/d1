console.log ('Hellow World');

// Functions
// [SECTIONS] Parameters and Arguments
	//Functions in Javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.

	function printName(name){
		console.log('My name is '+name);
		console.log('Hellow, '+name);
	};

	printName('Juana');
	// 'Juana' this is argument

	//DRY Principle
		//- Don't Repeat Yourself

	printName('Voren');

	// Variables can also be passed as an argument.

	let sampleVariable= 'Yui';
	printName(sampleVariable);

		// Let's try w prev act
		//num parameter while 64 is the argument

		function checkDivisibilityBy8(num){
			let remainder= num % 8;
			console.log('The remainder of '+ num + ' divided by 8 is '+ remainder);
			let isDivisibleBy8= remainder === 0;
			console.log('Is '+ num+ ' divisible by 8?');
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8(64);

		//[SECTION] Function as Arguments

		function argumentFunction(){
			console.log('This function was passed as an argument before the message was printed.');

		};

		//parameter always acts like a variable
		//parameter is like a catcher, holds value given by argument
		function invokeFunction (iAmNotRelated){
			iAmNotRelated();
		};

		invokeFunction(argumentFunction);
		//it's always from argument

		//[SUB-SECTION] Using Multiple Parameters

			//Multiple 'arguments' will correspond to the number of 'parameters' declared
			//in a function in succeeeding order.

			function createFullName(firstName, middleName, lastName){
				console.log(firstName+ ' '+ middleName+ ' '+lastName);
			};

			
		// [subsection] using variables as arguments

			let firstName= 'John';
			let middleName='Doe';
			let lastName= 'Smith';

			createFullName(firstName, middleName, lastName);

		
			function printFullName(middleName, firstName, lastName){
				console.log(firstName+ ' '+ middleName+ ' '+ lastName);
			};

			printFullName ('Juan', 'Crisostomo', 'Ibarra');

			// argument juan thrown to parameter middlename.
			//argument crisostomo thrown to firstName
			//ibarra thrown to lastname

			// thus, thru console log. Order is, Crisostomo Juan ibaara


		// [SECTION] Return Statement

			// The 'return' statement allows us to output a value from
			//a function to be passed to the line/block of code that invoke
			// /called the function.
				function returnFullName(firstName, middleName, lastName){
					console.log('This message will be printed.');
					return middleName+ ' '+firstName +' '+lastName; //return is like console.log but disables following codes after
					console.log('Try this one out!');
				};

				let completeName= returnFullName('F', 'P', 'J');
				console.log(completeName);

				let anotherName= returnFullName('Jeffrey', 'Smith', 'Bezos');
				console.log(anotherName);

				//this way, a fuinction is able to return a value we can
				//further use/manipulate in our program instead of
				//only printing/displaying it in the console.

				function returnAddress(city, country){
					let fullAddress = city+ ', '+ country+ '!';
					console.log('Welcome to Cebu!');
					return fullAddress;

				};

				let myAddress= returnAddress('Cebu City', 'Philippines');
				console.log(myAddress);

				//on the other hand, when a functio nhas console.log()
				//to display its result it will return undefined instead.


				function printPlayerInfo(username, level, job){
					console.log('Username: '+ username);
					console.log('Level: '+ level);
					console.log('Job: '+ job);

					let playerName= 'Username: '+ username;
					return playerName;
				};

				let user1= printPlayerInfo('knight_white', 95, 'Paladin');
				console.log(user1);

				//returns undefined because printPlayerInfo returns nothing
				//it only consoles.logs the details

				// you cannot save any value from pirintpLayerFO because
				//it does not return anyhting